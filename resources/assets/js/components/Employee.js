import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Employee extends Component {
	constructor(props) {
        super(props);

        this.state = {
            engineers: [],
            supervisors:[],
            selectedSupervisor:[]
          
        }
    }

    componentDidMount() {
        fetch('/api/engineers')
        .then(response => {

            return response.json();
        })
        .then(response => {
           const engineers = response.data;
           this.setState({ engineers: engineers });
       });


       
    }

    handleChange(e){
        // this.setState({selectedCountry: e.target.value});

     
        let supervisor = this.state.engineers.filter(data => {
            return data.id == e.target.value
        })
        console.log(supervisor);

        this.setState({ selectedSupervisor: supervisor }, () => {
            console.log(this.state.selectedSupervisor);
        }); 
        
        // console.log(this.state.selectedCountry);

    }
    renderEngineers() {
        return this.state.engineers.map((data,i) => {
            return (
                <option key={data.id} value={data.id}>{data.name} </option>
                );
            });
        }
 renderSupervisor() {
     return this.state.selectedSupervisor.map((data,i) => {
                console.log(data);
                return (
                <option key={data.supervisor.id} value={data.id}>{data.supervisor.name}</option>
                );
            })
 }

    render() {
      
    return (
    <div className="two fields">
     <div className="field" id="engineer">
    <label>Employee Name</label>
    <select className="ui fluid dropdown" onChange={this.handleChange.bind(this)} name="engineer_id"> 
    <option value="">Employee Name</option>
    { this.renderEngineers() }
    </select>
    </div>
      <div className="field">
                <label>Supervisor</label>


    <select className="ui fluid dropdown" name="supervisor_id">
     { this.renderSupervisor() }
  </select>
  </div>
  </div>
  );
}
}

export default Employee;


if (document.getElementById('emp-supervisor')) {
    ReactDOM.render(<Employee />, document.getElementById('emp-supervisor'));
}
