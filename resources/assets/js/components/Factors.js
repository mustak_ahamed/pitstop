import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import ScoreCard from './ScoreCard';


class Factors extends Component {
	constructor(props) {
        super(props);

        this.state = {
            calls: [],
            emails:[],
            ce:[],
            scores:[],
        }
    }

    componentDidMount() {
        fetch('/api/factors')
        .then(response => {

            return response.json();
        })
        .then(response => {
           const data = response.data;

           let call = data.filter(data => {
            return data.type == 'call'
        });

           this.setState({ calls: call }, () => {
            console.log(this.state.calls);
        }); 
           let email = data.filter(data => {
            return data.type == 'email'
        });
           this.setState({ emails: email }, () => {
            console.log(this.state.emails);
        }); 

           let ce = data.filter(data => {
            return data.type == 'ce'
        });
           this.setState({ ce: ce }, () => {
            console.log(this.state.ce);
        }); 


       });



    }
    saveComments(e) {
        const name = e.target.name;
            const factor_id = name.substring(name.indexOf("_") + 1);
            const comment = e.target.value;
            const evaluation_id = $('#evaluation_id').val();
            
            if(evaluation_id && comment) {
               $.ajax({
                  type: "POST",
                  url: "/api/saveScoreDrafts",
                  data: {
                    factor_id:factor_id,
                    evaluation_id:evaluation_id,
                    comment:comment
                },

                  success: function() {
                   
                },

               
            });
        }
    }
    renderCalls() {
        return this.state.calls.map((data,i) => {
            return (
                <div className="three fields" key={data.id}>
                <div className="field">
                <label>{data.factors}</label>

                </div>

                <div className="field">
                <SelectOptions data={data.id} callback={this.updateScoreDraft.bind(this)} />
                </div>


                <div className="field">
                <input type="text" placeholder="Comments" name={"comment_"+data.id}  onBlur= {this.saveComments.bind(this)} />
                </div>

                </div> 
                );
            })
        }


        updateScoreDraft(e){
            const name = e.target.name;
            const factor_id = name.substring(name.indexOf("_") + 1);
            const score = e.target.value;
            const evaluation_id = $('#evaluation_id').val();
            
            if(evaluation_id) {
               $.ajax({
                  type: "POST",
                  url: "/api/saveScoreDrafts",
                  data: {
                    factor_id:factor_id,
                    score:score,
                    evaluation_id:evaluation_id
                },

                success: function() {
                    fetch('/api/scoreResult/'+evaluation_id)
                    .then(response => {

                      return response.json();
                    })
                    .then(response => {
                     const data = response;
                     this.setState({ scores: data }, () => {
                      console.log(this.state.scores);
                    }); 
                     
                });  

                   
               }.bind(this),

               
            });
        }

    }

        renderScores() {
              console.log(this.state.scores);
              return (
              <ScoreCard scores = {this.state.scores} />
              );
       }
        renderEmails() {
            return this.state.emails.map((data,i) => {
                return (
                <div className="three fields" key={data.id}>
                <div className="field">
                <label>{data.factors}</label>

                </div>

                <div className="field">
                <SelectOptions data={data.id} callback={this.updateScoreDraft.bind(this)} />
                </div>


                <div className="field">
                <input type="text" placeholder="Comments" name={"comment_"+data.id} />
                </div>

                </div> 
                );
            })
        }

        renderCE() {
            return this.state.ce.map((data,i) => {
                return (
                <div className="three fields" key={data.id}>
                <div className="field">
                <label>{data.factors}</label>

                </div>

                <div className="field">

                <SelectOptions data={data.id} callback={this.updateScoreDraft.bind(this)} />

                </div>


                <div className="field">
                <input type="text" placeholder="Comments" name={"comment_"+data.id} />
                </div>

                </div> 
                );
            })
        }


        render() {

            return (
            <div>
              {this.renderScores()}

            <div className="ui bottom attached tab segment active" data-tab="first">




            <div className="ui center aligned segment container form callsDiv">


            <div className="ui centered header">
            <h1 className="font callsHead">Evaluation based on Calls</h1>
            </div>

            {this.renderCalls()}


            </div>
            </div>
            <div className="ui bottom attached tab segment" data-tab="second">




            <div className="ui center aligned segment container form">


            <div className="ui centered header">
            <h1 className="font callsHead">Evaluation based on Emails</h1>
            </div>

            {this.renderEmails()}


            </div>
            </div>
            <div className="ui bottom attached tab segment" data-tab="third">




            <div className="ui center aligned segment container form">


            <div className="ui centered header">
            <h1 className="font callsHead">Evaluation based on CE</h1>
            </div>

            {this.renderCE()}


            </div>
            </div>
            </div>

            );
        }
    }

    class SelectOptions extends React.Component {

 
    render() {
      return (
     
      <select className="ui fluid dropdown factorsSelect" name={"factor_"+this.props.data} onChange={this.props.callback}>
      <option value="">NA</option>
      <option value="2">Yes</option>
      <option value="1">Yes/Improvement</option>
      <option value="0">No</option>

      </select>
      );
  }
}


export default Factors;


if (document.getElementById('factors_container')) {
    ReactDOM.render(<Factors />, document.getElementById('factors_container'));
}



