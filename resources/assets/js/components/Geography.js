import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Geography extends Component {
	constructor(props) {
        super(props);

        this.state = {
            geography: [],
            project: [],
            listedProjects:[]

        }
    }

    componentDidMount() {
        fetch('/api/geography')
        .then(response => {

            return response.json();
        })
        .then(response => {
           const geography = response.data;
           this.setState({ geography: geography });
       });

        fetch('/api/project')
        .then(response => {

            return response.json();
        })
        .then(response => {
           const project = response.data;
           this.setState({ project: project });
       });



    }

    handleChange(e){
        // this.setState({selectedCountry: e.target.value});

        this.setState({ selectedCountry: e.target.value }, () => {
            // console.log(this.state.selectedCountry);
        }); 

        let projects = this.state.project.filter(data => {
            return data.geography_id == e.target.value
        })
        // console.log(projects);

        this.setState({ listedProjects: projects }, () => {
            console.log(this.state.listedProjects);
        }); 
        
        // console.log(this.state.selectedCountry);

    }
    renderCountries() {
        return this.state.geography.map((data,i) => {
            return (
                <option key={data.id} value={data.id}>{data.country} </option>
                );
            })
        }

    renderProjects() {
            return this.state.listedProjects.map((data,i) => {
                console.log(data);
                return (
                <option key={data.id} value={data.id}>{data.project}</option>
                );
            })
    }

    render() {
     
    return (
    <div className="two fields">
     <div className="field" id="geography">
    <label>Geography</label>
    <select className="ui fluid dropdown" onChange={this.handleChange.bind(this)} name="geography_id"> 
    <option value="">Geography</option>
    { this.renderCountries() }
    </select>
    </div>
      <div className="field">
                <label>Project</label>


    <select className="ui fluid dropdown" name="project_id">
     { this.renderProjects() }
  </select>
  </div>
  </div>
  );
}
}

export default Geography;


if (document.getElementById('geo-project')) {
    ReactDOM.render(<Geography />, document.getElementById('geo-project'));
}
