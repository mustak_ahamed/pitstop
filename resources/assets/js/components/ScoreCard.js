import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class ScoreCard extends Component {
	constructor(props) {
        super(props);

        this.state = {
           
        }
    }
   
   
    render() {
     
    return (
        <div>
        <div className="ui four column grid container center aligned">
        <div className="column ui card">

        <div className="ui statistics">
          <div className="statistic">
            <div className="value">
              {this.props.scores.callScores ? this.props.scores.callScores : 0 }%
            </div>
            <div className="label">
              Call
            </div>
          </div>

        </div>
      </div>
      <div className="column ui card">

        <div className="ui statistics">
          <div className="statistic">
            <div className="value">
              {this.props.scores.emailScores ? this.props.scores.emailScores : 0 }%
            </div>
            <div className="label">
              Email
            </div>
          </div>

        </div>
      </div>
      <div className="column ui card">

        <div className="ui statistics">
          <div className="statistic">
            <div className="value">
              {this.props.scores.ceScores ? this.props.scores.ceScores : 0}%
            </div>
            <div className="label">
              CE
            </div>
          </div>

        </div>
      </div>
      <div className="column ui card">

        <div className="ui statistics">
          <div className="statistic">
            <div className="value">
              {this.props.scores.totalScores ? this.props.scores.totalScores : 0 }%
            </div>
            <div className="label">
              Overall
            </div>
          </div>

        </div>
      </div>
    </div>
    <div className="ui top attached tabular menu">
    <a className="item active" data-tab="first">Calls</a>
    <a className="item" data-tab="second">Email</a>
    <a className="item" data-tab="third">CE</a>
    <div className="right menu">
     <div className="timer"><span className="time-loaders"><time>00:00:00</time></span> 
     </div>
    </div>
       </div>
         
    </div>
  );
}
}

export default ScoreCard;


