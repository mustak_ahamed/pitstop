@extends('layouts.main')

@section('content')
<div class="ui three column grid container center aligned padding-top-golden">

<div class="ui grid padding-top-golden">
        <div class="sixteen wide column">
        	<div class="ui cards">
        		<div class="ui card material-drop">
        		  <div class="content">
        		    <a href="#" class="header">Evaluation</a>
        		    <div class="description">
        		     
        		    </div>
        		  </div>
        		    <div class="extra content">
        		    <a>
        		      <i class="book icon"></i>
        		      2 Hours
        		    </a>
        		  </div>
        		</div>
        		<div class="ui card material-drop">
        		  <div class="content">
        		    <a href="#" class="header">Coaching</a>
        		    <div class="description">
        		     
        		    </div>
        		  </div>
        		    <div class="extra content">
        		    <a>
        		      <i class="book icon"></i>
        		      2 Hours
        		    </a>
        		  </div>
        		</div>
        		<div class="ui card material-drop">
        		  <div class="content">
        		    <a href="#" class="header">Module A</a>
        		    <div class="description">
        		     
        		    </div>
        		  </div>
        		    <div class="extra content">
        		    <a>
        		      <i class="book icon"></i>
        		      3 Hours
        		    </a>
        		  </div>
        		</div>
        		<div class="ui card material-drop">
        		  <div class="content">
        		    <a href="#" class="header">Module B</a>
        		    <div class="description">
        		    </div>
        		  </div>
        		    <div class="extra content">
        		    <a>
        		      <i class="book icon"></i>
        		      7 Hours
        		    </a>
        		  </div>
        		</div>
        		<div class="ui card material-drop">
        		  <div class="content">
        		    <a href="#" class="header">Module C</a>
        		    <div class="description">
        		    </div>
        		  </div>
        		   <div class="extra content">
        		    <a>
        		      <i class="book icon"></i>
        		      1 Hours
        		    </a>
        		  </div>
        		</div>
        		<div class="ui card material-drop">
        		  <div class="content">
        		    <a href="#" class="header">Module D</a>
        		    <div class="description">
        		    </div>
        		  </div>
        		  <div class="extra content">
        		    <a>
        		      <i class="book icon"></i>
        		      2 Hours
        		    </a>
        		  </div>
        		</div>
        	</div>
        	
        </div>
    </div>
    </div>
@endsection