      <a href="#" class="header item">
        {{-- <img class="logo" src="assets/images/logo.png"> --}}
        PIT STOP
      </a>


      @role('QA')
      <a href="{{ url('qa/evaluated') }}" class="item">Home</a>
      @endrole

      @role('Engineer')
      <a href="{{ url('engineer/dashboard') }}" class="item">Home</a>
      
      @endrole

      @role('Lead')
      <a href="{{ url('lead/dashboard') }}" class="item">Home</a>
      @endrole

       @role('Admin')
      <a href="{{ url('/users') }}" class="item">Admin</a>
      @endrole


      <div class="ui simple dropdown item">
        Pages <i class="dropdown icon"></i>
        <div class="menu">

          @role('Engineer')
          <a href="{{ url('engineer/analytics') }}" class="item">Analytics</a>
          <div class="item">
            <i class="dropdown icon"></i>
            Dashboard
            <div class="menu">
              <a class="item" href="{{ url('engineer/dashboard') }}">Evaluations</a>
              <a class="item" href="{{ url('engineer/pending') }}">Pending</a>
            </div>
          </div>

          @endrole

          @role('QA')
          <a href="{{ url('qa/evaluated') }}" class="item">Dashboard</a>
          <a href="{{ url('qa/evaluation') }}" class="item">Evaluation</a>
          

          @endrole

          @role('Lead')
          <a href="{{ url('lead/dashboard') }}" class="item">Dashboard</a>
          @endrole



          {{-- <div class="divider"></div> --}}
          {{-- <div class="header">Header Item</div> --}}

        </div>
      </div>