<style type="text/css">
  .chart {
    position: relative;
    display: inline-block;
    width: 110px;
    height: 110px;
    margin-top: 50px;
    margin-bottom: 50px;
    text-align: center;
  }
  .chart canvas {
    position: absolute;
    top: 0;
    left: 0;
  }
  .percent {
    display: inline-block;
    line-height: 110px;
    z-index: 2;
  }
  .percent:after {
    content: '%';
    margin-left: 0.1em;
    font-size: .8em;
  }


</style>
<div class="ui four column grid container center aligned">
  <div class="column ui fluid card">

   <span class="chart" data-percent="{{ (float)$scoreDetails['callScores'] }}">
    <span class="percent"></span>
  </span>
  <div class="label">
    Call      
  </div>
</div>
<div class="column ui card">

 <span class="chart" data-percent="{{ (float)$scoreDetails['emailScores'] }}">
  <span class="percent"></span>
</span>

<div class="label">
  Email
</div>
</div>
<div class="column ui card">
 <span class="chart" data-percent="{{ (float)$scoreDetails['ceScores'] }}">
  <span class="percent"></span>
</span>

<div class="label">
  CE
</div>

</div>
<div class="column ui card">
 <span class="chart" data-percent="{{ (float)$scoreDetails['totalScores'] }}">
  <span class="percent"></span>
</span>

<div class="label">
  Overall
</div>

</div>
</div>
<h4 class="ui horizontal divider header">
  <i class="tag icon"></i>
  Details of Evaluation
</h4>


<div class="ui styled fluid accordion">
  @if(isset($timerOption))
  <div class="right menu">
   <div class="timer"><span class="time-loaders"><time>00:00:00</time></span> 
   </div>
 </div>
 @endif
 <div class="active title">
  <i class="dropdown icon"></i>
  Basic Informations

</div>
<div class="active content">
 <table class="ui inverted grey table">
  <thead>
    <tr>
      <th colspan="2">Evaluation Information</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Geography: </td><td>{{ $evaluationDetail->geography->country }}</td>
    </tr>
    <tr>
      <td>Project: </td><td>{{ $evaluationDetail->project->project }}</td>
    </tr>
    <tr>
      <td>Language Type: </td><td>{{ $evaluationDetail->language->language }}</td>
    </tr>
    <tr>
      <td>LOB: </td><td>{{ $evaluationDetail->lob->lob }}</td>
    </tr>
    <tr>
      <td>Monitoring Date: </td><td>{{ $evaluationDetail->monitoring_date }}</td>
    </tr>
    <tr>
      <td>Date of Evaluation: </td><td>{{ $evaluationDetail->date_of_evaluation }}</td>
    </tr>
    <tr>
      <td>Call ID: </td><td>{{ $evaluationDetail->call_id }}</td>
    </tr>
    <tr>
      <td>Call Date: </td><td>{{  $evaluationDetail->call_date }}</td>
    </tr> 
    <tr>
      <td>Call Duration: </td><td>{{  $evaluationDetail->call_duration }}</td>
    </tr> 
    <tr>
      <td>Employee Name: </td><td> {{ $evaluationDetail->engineer->name }}</td>
    </tr> 
    <tr>
      <td>Supervisor Name: </td><td>{{ $evaluationDetail->supervisor->name }}</td>
    </tr> 
    <tr>
      <td>Quality Analyst: </td><td>{{ $evaluationDetail->qa->name }}</td>
    </tr> 
    @role('Lead')
     <tr>
      <td>Evaluation Duration: </td><td>{{ $evaluationDetail->evaluation_duration }}</td>
    </tr> 
     <tr>
      <td>Coaching Duration: </td><td>{{ $evaluationDetail->coaching_duration }}</td>
    </tr> 
    @endrole

  </tbody>
</table>
</div>
<div class="title">
  <i class="dropdown icon"></i>
  Score Details
</div>
<div class="content">
  <div class="column">
    <div class="ui segment">
      <div class="ui top attached tabular menu">
        <a class="item active" data-tab="first">Calls</a>
        <a class="item" data-tab="second">Email</a>
        <a class="item" data-tab="third">CE</a>
      </div>
      <div class="ui bottom attached tab segment active" data-tab="first">

        <div class="ui segment purple material-drop">
          <h3 class="ui dividing header">
            Evaluation in Calls
          </h3>
          <table class="ui table celled blue">
            <thead>
              <tr>
                <th>Factors</th>
                <th>Scores</th>
                <th>Comments</th>
              </tr>
            </thead>
            <tbody>
              @foreach($evaluationDetail->evaluationScore as $evaluation)

              @if($evaluation->evaluationFactor->type == 'call')
              <tr>


                <td>{{ $evaluation->evaluationFactor->factors }}</td>
                <td>
                  @if($evaluation->score == '2')
                  Yes
                  @elseif($evaluation->score == '1')
                  Yes/Improvement
                  @elseif($evaluation->score == '0')
                  No
                  @else
                  NA
                  @endif
                </td>
                <td>{{ $evaluation->comment }}</td>
              </tr>
              @endif

              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="ui bottom attached tab segment" data-tab="second">
        <div class="ui segment purple material-drop">
          <h3 class="ui dividing header">
            Evaluation in Emails
          </h3>
          <table class="ui table celled blue">
            <thead>
              <tr>
                <th>Factors</th>
                <th>Scores</th>
                <th>Comments</th>
              </tr>
            </thead>
            <tbody>
              @foreach($evaluationDetail->evaluationScore as $evaluation)

              @if($evaluation->evaluationFactor->type == 'email')
              <tr>


                <td>{{ $evaluation->evaluationFactor->factors }}</td>
                <td>
                 @if($evaluation->score == '2')
                 Yes
                 @elseif($evaluation->score == '1')
                 Yes/Improvement
                 @elseif($evaluation->score == '0')
                 No
                 @else
                 NA
                 @endif
               </td>
               <td>{{ $evaluation->comment }}</td>
             </tr>
             @endif

             @endforeach
           </tbody>
         </table>
       </div>
     </div>
     <div class="ui bottom attached tab segment" data-tab="third">
      <div class="ui segment purple material-drop">
        <h3 class="ui dividing header">
          Evaluation in CE
        </h3>
        <table class="ui table celled blue">
          <thead>
            <tr>
              <th>Factors</th>
              <th>Scores</th>
              <th>Comments</th>
            </tr>
          </thead>
          <tbody>
            @foreach($evaluationDetail->evaluationScore as $evaluation)

            @if($evaluation->evaluationFactor->type == 'ce')
            <tr>


              <td>{{ $evaluation->evaluationFactor->factors }}</td>
              <td>
                @if($evaluation->score == '2')
                Yes
                @elseif($evaluation->score == '1')
                Yes/Improvement
                @elseif($evaluation->score == '0')
                No
                @else
                NA
                @endif
              </td>
              <td>{{ $evaluation->comment }}</td>
            </tr>
            @endif

            @endforeach
          </tbody>
        </table>
      </div>


    </div>     
    <h4> Overall Comments: </h4>  
    <p> {{ $evaluationDetail->qa_comment }}</p>
  </div>

</div>
</div>

@if( $evaluationDetail->coaching_input)
<div class="title">
  <i class="dropdown icon"></i>
  Coach Details
</div>
<div class="content">
  <h4>Coaching Input: </h4> 
  <p> {{ $evaluationDetail->coaching_input }} </p>
  <p><span>Agent has been coached in Person TL/QA?:</span> <b> @if($evaluationDetail->qa_coach_acceptance == 1) Yes @else No @endif </b>
  </div>
  @endif

  @if( $evaluationDetail->engineer_status)
  <div class="title">
    <i class="dropdown icon"></i>
    Engineer's Comments
  </div>
  <div class="content">
    <h4>Comment: </h4> 
    <p> {{ $evaluationDetail->engineer_comment }} </p>

    <p><span>The Evaluation of this transaction has stated that they have coached you for this transaction in person. Please confirm whether you have been coached in person?:</span> <b> @if($evaluationDetail->engineer_coach_acceptance == 1) Yes @else No @endif </b>

      <p>Acceptance: <b>@if($evaluationDetail->engineer_status == 1) Accepted @elseif($evaluationDetail->engineer_status == 2) Escalated @endif</b></p>

    </div>
    @endif

  </div>





  <script type="text/javascript">

    $(function() {
      $('.chart').easyPieChart({
        easing: 'easeOutBounce',
        barColor: '#69c',
        trackColor: '#ace',
        scaleColor: false,
        lineWidth: 20,
        trackWidth: 16,
        lineCap: 'butt',
        onStep: function(from, to, percent) {
          $(this.el).find('.percent').text(Math.round(percent));
        }
      });
      var chart = window.chart = $('.chart').data('easyPieChart');

    });
  </script>
