@extends('layouts.main')

@section('content')



@include ('elements.evaluationScore')


<h4 class="ui horizontal divider header">
  <i class="book icon"></i>
  Coach Details
</h4>

<div class="ui grid container aligned">

  <div class="column ui card">

    <form class="ui form" method="POST" action="{{ url('qa/coach') }}">
      {{ csrf_field() }}
      <div class="field">
        <label>Coaching Input</label>
        <textarea rows="2" name="coaching_input"></textarea>
      </div>
      <input type="hidden" name="evaluation_id" value="{{ $evaluationDetail->id }}">

      <div class="ui form">
        <div class="inline fields">
          <label>Agent has been coached in Person TL/QA?</label>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" name="qa_coach_acceptance" checked="checked" value="1" tabindex="0">
              <label>Yes</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" name="qa_coach_acceptance" value="0" tabindex="0">
              <label>No</label>
            </div>
          </div>
          <input type="hidden" id="timerValue" name="coaching_duration">


        </div>
      </div>
      <div class="ui buttons">
        <button class="ui positive button" type="submit">Submit</button>
        
      </form>
    </div>
  </div>
  @endsection
  <script type="text/javascript">

    var seconds = 0, minutes = 0, hours = 0,t;

    function add() {
      // alert($('time-loaders').length);

      seconds++;
      if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
          minutes = 0;
          hours++;
        }
      }

      var textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);
    // alert(textContent);
    // document.getElementsByClassName("time-loaders").textContent=textContent;
    $('.time-loaders').text(textContent);
    $('#timerValue').val(textContent);
    timer();
  }
  function timer() {
    t = setTimeout(add, 1000);
  }
  timer();


</script>