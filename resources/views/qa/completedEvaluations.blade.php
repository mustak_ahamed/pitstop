@extends('layouts.main')

@section('content')

<div class="ui segment">
	<h3>Completed Evaluations</h3>
	<table class="ui single line striped selectable table datatable">
		<thead>
			<tr>
				
				<th>Project</th>
				<th>Engineer</th>
				<th>Date of Evaluation</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>

		@foreach($evaluations as $evaluation)
			<tr>
				<td>
					{{ $evaluation->project->project }}
				</td>
				<td>
					{{ $evaluation->engineer->name }}
				</td>
				<td>
					
					{{ $evaluation->date_of_evaluation->toFormattedDateString() }}

				</td>
				<td>
					<a href="coach/{{ $evaluation->id }}" class="ui button blue">
						Coach
					</a>
				</td>
			
			</tr>

			@endforeach
			
		</tbody>
	</table>
</div>

@endsection