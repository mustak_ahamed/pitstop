@extends('layouts.main')
<style type="text/css">
  .in, .out {
    -webkit-animation-timing-function: ease-in-out;
    -webkit-animation-duration: 500ms !important;
  }

  .font{

    font-weight:300;
    font-size:40px;
  }


  .text.container{

    width:40%;

  }


  #logo{
    border:2px solid white;
    border-radius: 10px;
    color:white;
    padding:15px;
  }
  #logo:hover{
    box-shadow: 0px 0px 5px 5px white;
  }

  div [class*="left floated"] {
    float: right;
    margin-left: 0.25em;
  }
  .callsDiv{
    background-color: #F1F0FF;
    border-radius:5px;
  }
  .callsHead {
    color:#300032;
  }


</style>
@section('content')

<div class="ui one column grid container">
  <div class="ui column card" id="monitor">
    <form class="ui form" method="POST" action="{{ url('qa/evaluation') }}" id="evaluationForm" enctype='multipart/form-data'>
      {{ csrf_field() }}

      <div class="ui grid container">

        <div class="row"></div>
        <div class="row"></div>


        <div class="ui text container">
          <div class="two fields">
            <div class="field">

              <h4 class="ui dividing header">Transaction Moinitoring Form </h4>
            </div>
            <div class="field">
              <div class="timer"><span class="time-loaders"><time>00:00:00</time></span> 
              </div> </div>

            </div>
            <div id="geo-project">




            </div>
            <div class="two fields">
              <div class="field">
                <label>Language Type</label>
                <select class="ui fluid dropdown" name="language_id">
                  <option value="">Language</option>
                  @foreach ($languages as $language)
                  <option value={{ $language->id }}>{{ $language->language }}</option>

                  @endforeach

                </select>
              </div>
              <div class="field">
                <label>LOB</label>
                <select class="ui fluid dropdown" name="lob_id">
                  <option value="">LOB</option>
                  @foreach ($lob as $lob)
                  <option value={{ $lob->id }}>{{ $lob->lob }}</option>

                  @endforeach
                </select>
              </div>
            </div>
            <div class="field">
              <div class="two fields">
                <div class="field">
                 <label>Monitoring Date</label>
                 {{-- <input type="text" class="datepicker" name="shipping[first-name]" placeholder="First Name"> --}}
                 <div class="ui calendar">
                  <div class="ui input left icon">
                    <i class="calendar icon"></i>
                    <input type="text" value="{{ date('F d,Y H:i:s') }}" name="monitoring_date" readonly="readonly">
                  </div>
                </div>
              </div>
              <div class="field">
               <label>Date of Evaluation </label>
               {{-- <input type="text" class="datepicker" name="shipping[last-name]" placeholder="Last Name"> --}}
               <div class="ui calendar">
                <div class="ui input left icon">
                  <i class="calendar icon"></i>
                  <input type="text" value="{{ date('F d,Y H:i:s') }}" name="date_of_evaluation" readonly="readonly">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="field">
          <div class="three fields">
            <div class="field">
             <label>Call ID</label>
             <input type="text" name="call_id" placeholder="Call ID">
           </div>
           <div class="field">
             <label>Call Date</label>
             {{-- <input type="text" clss="datepicker" name="shipping[first-name]" placeholder="First Name"> --}}
             <div class="ui calendar datepicker">
              <div class="ui input left icon">
                <i class="calendar icon"></i>
                <input type="text" name="call_date" placeholder="Date/Time">
              </div>
            </div>
          </div>
          <div class="field">
           <label>Call Duration</label>
           <div class="two fields">
            <div class="field">
             <input type="number" name="hours" placeholder="hours" min="0" max="23">
           </div> :
           <div class="field">
             <input type="number" name="minutes" placeholder="minutes" min="0" max="59">
           </div>
         </div>
         
       </div>
     </div>
   </div>
   <div class="field" id="emp-supervisor">

   </div>
   <div class="three fields">
     <div class="field">
       <label>Quality Analyst</label>
       <div class="ui transparent input">
         <input type="text" name='qa' value={{ Auth::user()->name }} placeholder="Quality Analyst" readonly="readonly">
         <input type="hidden" name='qa_id' value={{ Auth::user()->id }}>
       </div>
     </div>

     <div class="field">
      <input type="file" name='evaluationDoc' id="upload_file">
    </div>

    <div class="field">
         {{-- <div class="ui buttons">
          <button class="ui button next1" type="button">Cancel</button>
          <div class="or"></div>
          <button class="ui positive button" type="submit">Save</button>
        </div> --}}
        <div class="ui buttons">
          <button class="ui right labeled icon button next1" type="submit">
            <i class="right arrow icon"></i>
            Save & Next
          </button>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</div>

<div class="ui column card" id="factorList">

  <div id="form2"> </div>
  <form class="ui form" method="POST" action="{{ url('qa/saveScore') }}" id="scoreForm">

    <div id="factors_container">


    </div>
    <div class="grid container">

      <input type="hidden" name='evaluation_id' value="" id="evaluation_id" ref="evaluationId">

      <div class="field">
        <label>Overall Comments</label>
        <textarea rows="2" name="qa_comment"></textarea>
      </div>
      <input type="hidden" id="timerValue" name="evaluation_duration">

      <div class="ui buttons">
        <button class="ui labeled icon button prev1" type="button">
          <i class="left chevron icon"></i>
          Back
        </button>
        <div class="or"></div>
        <button type="submit" class="ui fluid teal submit button" type="submit">
          Finish
        </button>
      </div>
    </form>
  </div>


</div>

</form>
</div>

@endsection
<script type="text/javascript">
  var seconds = 0, minutes = 0, hours = 0,t;

  function add() {
    seconds++;
    if (seconds >= 60) {
      seconds = 0;
      minutes++;
      if (minutes >= 60) {
        minutes = 0;
        hours++;
      }
    }
    
    var textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);
    // alert(textContent);
    // document.getElementsByClassName("time-loaders").textContent=textContent;
    $('.time-loaders').text(textContent);
    $('#timerValue').val(textContent);
    timer();
  }
  function timer() {
    t = setTimeout(add, 1000);
  }
  timer();


</script>
<style type="text/css">


  #factorList{
    display: none;
  }

  #monitor{
    display: block;
  }

</style>
