<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
{{--     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
--}}    
<link rel="stylesheet" href="{{ asset('css/semantic-ui.css') }}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css">
 body {
  background-color: #DADADA;
}
</style>

</head>
<body>
    <div id="app">
        

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/semantic-ui.js') }}"></script>
    <script type="text/javascript">
      $(document).ready(function() {
          $('.ui.form')
          .form({
              fields: {
                email: {
                  identifier  : 'email',
                  rules: [
                  {
                      type   : 'empty',
                      prompt : 'Please enter your email'
                  },
                // {
                //   type   : 'email',
                //   prompt : 'Please enter a valid e-mail'
                // }
                ]
            },
            password: {
              identifier  : 'password',
              rules: [
              {
                  type   : 'empty',
                  prompt : 'Please enter your password'
              },
              {
                  type   : 'length[6]',
                  prompt : 'Your password must be at least 6 characters'
              }
              ]
          }
      }
  });        

      });
      
  </script>

</body>
</html>
