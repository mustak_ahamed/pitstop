@extends('master')

<style type="text/css">
   
  body > .grid {
      height: 100%;
  }
  .image {
      margin-top: -100px;
  }
  .column {
      max-width: 450px;
       margin-top: 150px;
  }
</style>

@section('content')
{{-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      {{-- <img src="assets/images/logo.png" class="image"> --}}
      <div class="content">
        Log-in to your account
    </div>
</h2>
<form class="ui large form" method="POST" action="{{ route('login') }}">
     {{ csrf_field() }}
  <div class="ui stacked segment">
    <div class="field">
      <div class="ui left icon input">
        <i class="user icon"></i>
        <input type="email" name="email" placeholder="User Id">
             @if ($errors->has('email'))
                <div class="ui left pointing red basic label">Please enter your email</div>
            @endif
    </div>
</div>
<div class="field">
  <div class="ui left icon input">
    <i class="lock icon"></i>
    <input type="password" name="password" placeholder="Password">
</div>
</div>
{{-- <div class="ui fluid large teal submit button">Login</div> --}}
 <button type="submit" class="ui fluid large teal submit button">
                                    Login
                                </button>
</div>

<div class="ui error message"></div>

</form>

{{-- <div class="ui message">
  New to us? <a href="#">Sign Up</a>
</div> --}}
</div>
</div>
@endsection
