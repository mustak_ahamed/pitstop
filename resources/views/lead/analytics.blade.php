@extends('layouts.main')

@section('content')
<div class="ui grid">
	<div class="sixteen wide column card">
		<div id="container" style="width: 75%;">
			<canvas id="canvas"></canvas>
		</div>
	</div>
	
	<div class="four wide column"></div>
	<div class="four wide column"></div>

</div>
@endsection

<script src="{{ asset('js/chartjs/Chart.bundle.js') }}"></script>

<script src="{{ asset('js/chartjs/util.js') }}"></script>
<script>
		var color = Chart.helpers.color;
		var barChartData = {
			labels: ["Mustak", "Rohan", "Gokul", "Sathya"],
			datasets: [{
				label: 'Call',
				backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
				borderColor: window.chartColors.red,
				borderWidth: 1,
				data: [
				89,
				96,
				25,
				45,
				66,
				15,
				86
				]
			}, {
				label: 'Email',
				backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
				borderColor: window.chartColors.blue,
				borderWidth: 1,
				data: [
				35,
				45,
				12,
				22,
				99,
				96,
				36
				]
			},
			{
				label: 'CE',
				backgroundColor: color(window.chartColors.yellow).alpha(0.5).rgbString(),
				borderColor: window.chartColors.yellow,
				borderWidth: 1,
				data: [
				35,
				45,
				12,
				22,
				99,
				96,
				36
				]
			}]

		};


	window.onload = function() {
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myBar = new Chart(ctx, {
			type: 'bar',
			data: barChartData,
			options: {
				responsive: true,
				legend: {
					position: 'top',
				},
				title: {
					display: true,
					text: "Engineer's Score Analysis in this Week"
				}
			}
		});
		
	};



	var colorNames = Object.keys(window.chartColors);





</script>