@extends('layouts.main')

@section('content')

<div class="ui pointing menu">
	<a class="item active link" href="#">
		<i class="icon database"></i>
		Team Utilities
	</a>
	{{-- <a class="item link" href="#">
		<i class="icon database"></i>
		My Pending signoff
	</a> --}}
	<div class="right menu">

		<a href="export" class="item">Export</a>
	</div>
	
	
	
</div>
<div class="ui segment">
	<h3>All Evaluations</h3>
	<table class="ui celled table datatable">
		<thead>
			<tr>
				
				<th>Project</th>
				<th>Engineer</th>
				<th>Quality Analyst</th>
				<th>Date of Evaluation</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			@foreach($evaluations as $evaluation)

			<tr>
				<td>
					{{ $evaluation->project->project }}
				</td>
				<td>
					{{ $evaluation->engineer->name }}
				</td>
				<td>
					{{ $evaluation->qa->name }}
				</td>
				<td>
					
					{{ $evaluation->date_of_evaluation->toFormattedDateString() }}

				</td>
				<td>
					<a href="viewEvaluation/{{ $evaluation->id }}" class="ui button blue">
						View
					</a>
				</td>
			</tr>
			@endforeach

		</tbody>
	</table>
</div>

@endsection