<html>
<table class="ui celled table datatable">
		<thead>
			<tr>
				
				<th>Project</th>
				<th>Engineer</th>
				<th>Quality Analyst</th>
				<th>Date of Evaluation</th>
			</tr>
		</thead>
		<tbody>
			
			@foreach($evaluations as $evaluation)

			<tr>
				<td>
					{{ $evaluation->project->project }}
				</td>
				<td>
					{{ $evaluation->engineer->name }}
				</td>
				<td>
					{{ $evaluation->qa->name }}
				</td>
				<td>
					
					{{ $evaluation->date_of_evaluation->toFormattedDateString() }}

				</td>
				
			</tr>
			@endforeach

			
		</tbody>
	</table>
</html>