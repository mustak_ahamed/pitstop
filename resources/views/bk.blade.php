<div class="ui bottom attached tab segment active" data-tab="first">

      <div id="account">


        <div class="ui center aligned segment container form " id="signUpBox" style="background-color: #F1F0FF;border-radius:5px;">


          <div class="ui centered header">
            <h1 class="font" style="color:#300032;">Evaluation based on Calls</h1>
          </div>

          <div class="three fields">
            <div class="field">
              <label>Call Opening</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="factor_1">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments" name="comment_1">
            </div>

          </div>
          <div class="three fields">
            <div class="field">
              <label>Followed Hold Procedure/Dead Air</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>
          <div class="three fields">
            <div class="field">
              <label>Listening</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>
          <div class="three fields">
            <div class="field">
              <label>Comprehension</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>
          <div class="three fields">
            <div class="field">
              <label>Tone & Intonation</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>

          <div class="three fields">
            <div class="field">
              <label>Paraphrasing</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>
          <div class="three fields">
            <div class="field">
              <label>Appropriate Response</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>

          <div class="three fields">
            <div class="field">
              <label>Assurance/Empathy</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>

          <div class="three fields">
            <div class="field">
              <label>Courtesy/Professionalism</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>

          <div class="three fields">
            <div class="field">
              <label>Call Closing</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>

          <div class="three fields">
            <div class="field">
              <label>Sentence construction/Word order</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>

          <div class="three fields">
            <div class="field">
              <label>Fluency & Lexical resource</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>

          <div class="three fields">
            <div class="field">
              <label>Rate of Speech</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>

          <div class="three fields">
            <div class="field">
              <label>Pronunciation/Chunking</label>

            </div>

            <div class="field">
              <div class="ui checkbox">
                <input type="checkbox" name="example">
                <label>Yes</label>
              </div>
            </div>

            <div class="field">
              <input type="text" placeholder="Comments">
            </div>

          </div>

        </div>

      </div>
    </div>
    <div class="ui bottom attached tab segment" data-tab="second">
      <div id="social">


        <div class="ui center aligned  segment container form " id="signUpBox" style="background-color: #F1F0FF;border-radius:5px;">


          <div class="ui centered header">
            <h1 class="font" style="color:#300032;">Evaluation Based on Email's</h1>
          </div>

          <div class="three fields">
            <div class="field">
             <label>Mail Opening</label>

           </div>

           <div class="field">
            <div class="ui checkbox">
              <input type="checkbox" name="example">
              <label>Yes</label>
            </div>
          </div>

          <div class="field">
            <input type="text" placeholder="Comments">
          </div>

        </div>
        <div class="three fields">
          <div class="field">
           <label>Tone of the Email</label>

         </div>

         <div class="field">
          <div class="ui checkbox">
            <input type="checkbox" name="example">
            <label>Yes</label>
          </div>
        </div>

        <div class="field">
          <input type="text" placeholder="Comments">
        </div>

      </div>
      <div class="three fields">
        <div class="field">
         <label>Paraphrasing</label>

       </div>

       <div class="field">
        <div class="ui checkbox">
          <input type="checkbox" name="example">
          <label>Yes</label>
        </div>
      </div>

      <div class="field">
        <input type="text" placeholder="Comments">
      </div>

    </div>
    <div class="three fields">
      <div class="field">
       <label>Assurance/Empathy</label>

     </div>

     <div class="field">
      <div class="ui checkbox">
        <input type="checkbox" name="example">
        <label>Yes</label>
      </div>
    </div>

    <div class="field">
      <input type="text" placeholder="Comments">
    </div>

  </div>
  <div class="three fields">
    <div class="field">
     <label>Courtesy /Professionalism</label>

   </div>

   <div class="field">
    <div class="ui checkbox">
      <input type="checkbox" name="example">
      <label>Yes</label>
    </div>
  </div>

  <div class="field">
    <input type="text" placeholder="Comments">
  </div>

</div>
<div class="three fields">
  <div class="field">
   <label>Mail closing</label>

 </div>

 <div class="field">
  <div class="ui checkbox">
    <input type="checkbox" name="example">
    <label>Yes</label>
  </div>
</div>

<div class="field">
  <input type="text" placeholder="Comments">
</div>

</div>
<div class="three fields">
  <div class="field">
   <label>Grammar</label>

 </div>

 <div class="field">
  <div class="ui checkbox">
    <input type="checkbox" name="example">
    <label>Yes</label>
  </div>
</div>

<div class="field">
  <input type="text" placeholder="Comments">
</div>

</div>

</div>

</div>
</div>
<div class="ui bottom attached tab segment" data-tab="third">
  <div id="personal">


    <div class="ui center aligned  segment container form" id="signUpBox" style="background-color: #F1F0FF;border-radius:5px;">


      <div class="ui centered header">
        <h1 class="font" style="color:#300032;">Evaluation Based on CE</h1>
      </div>

      <div class="three fields">
        <div class="field">
          <label>Educating customer/Setting Expectations</label>

        </div>

        <div class="field">
          <div class="ui checkbox">
            <input type="checkbox" name="example">
            <label>Yes</label>
          </div>
        </div>

        <div class="field">
          <input type="text" placeholder="Comments">
        </div>

      </div>
      <div class="three fields">
        <div class="field">
          <label>Ownership/ Case hand off</label>

        </div>

        <div class="field">
          <div class="ui checkbox">
            <input type="checkbox" name="example">
            <label>Yes</label>
          </div>
        </div>

        <div class="field">
          <input type="text" placeholder="Comments">
        </div>

      </div>

      <div class="three fields">
        <div class="field">
          <label>Documentation</label>

        </div>

        <div class="field">
          <div class="ui checkbox">
            <input type="checkbox" name="example">
            <label>Yes</label>
          </div>
        </div>

        <div class="field">
          <input type="text" placeholder="Comments">
        </div>

      </div>


      <div class="three fields">
        <div class="field">
          <label>CSS Standards</label>

        </div>

        <div class="field">
          <div class="ui checkbox">
            <input type="checkbox" name="example">
            <label>Yes</label>
          </div>
        </div>

        <div class="field">
          <input type="text" placeholder="Comments">
        </div>

      </div>





    </div>

  </div>


</div>



<div className="ui bottom attached tab segment active" data-tab="second">

   


      <div className="ui center aligned segment container form">


        <div className="ui centered header">
          <h1 className="font callsHead">Evaluation based on Emails</h1>
        </div>

        {this.renderEmails()}
       

    </div>
  </div>
  <div className="ui bottom attached tab segment active" data-tab="third">

   


      <div className="ui center aligned segment container form">


        <div className="ui centered header">
          <h1 className="font callsHead">Evaluation based on CE</h1>
        </div>

        {this.renderCE()}
       

    </div>
  </div>