@extends('layouts.main')

@section('content')

<div class="ui pointing menu">
	<a class="item active link" href="dashboard">
		<i class="icon database"></i>
		My Evaluation
	</a>
	<a class="item link" href="pending">
		<i class="icon database"></i>
		My Pending signoff

		@if($pendingEvaluations)
		<div class="ui floating circular teal label">{{ $pendingEvaluations }}</div>
		@endif
		
	</a>
	<div class="right menu">

		{{-- <a class="item">Export</a> --}}
	</div>
	
	
	
</div>
<div class="ui segment">
	<h3>My Evaluations</h3>
	<table class="ui celled table datatable">
		<thead>
			<tr>
				
				<th>Project</th>
				<th>Quality Analyst</th>
				<th>Date of Evaluation</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			@foreach($evaluations as $evaluation)
			<tr>
				<td>
					{{ $evaluation->project->project }}
				</td>
				<td>
					{{ $evaluation->qa->name }}
				</td>
				<td>
					
					{{ $evaluation->date_of_evaluation->toFormattedDateString() }}

				</td>
				<td>
					<a href="myEvaluation/{{ $evaluation->id }}" class="ui button blue">
						View
					</a>
				</td>
				
			</tr>

			@endforeach
		</tbody>
	</table>
</div>

@endsection