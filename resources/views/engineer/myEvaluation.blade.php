@extends('layouts.main')

@section('content')

@include ('elements.evaluationScore')

@if(!$evaluationDetail->engineer_status)

<h4 class="ui horizontal divider header">
  <i class="tag icon"></i>
  Engineer's Comments 
</h4>

<div class="ui grid container aligned">
  <div class="column ui card">

    <form class="ui form" method="POST" action="{{ url('engineer/engEvaluationSubmit') }}">
      {{ csrf_field() }}
      <div class="field">
        <label>Comments</label>
        <textarea rows="2" name="engineer_comment"></textarea>
      </div>
      <input type="hidden" name="evaluation_id" value="{{ $evaluationDetail->id }}">

      <div class="ui form">
        <div class="inline fields">
          <label>The Evaluation of this transaction has stated that they have coached you for this transaction in person. Please confirm whether you have been coached in person?</label>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" name="engineer_coach_acceptance" checked="checked" value="1" tabindex="0">
              <label>Yes</label>
            </div>
          </div>
          <div class="field">
            <div class="ui radio checkbox">
              <input type="radio" name="engineer_coach_acceptance" value="0" tabindex="0">
              <label>No</label>
            </div>
          </div>
          
        </div>
      </div>
      <div class="ui buttons">
        <button class="ui positive button" name="engineer_status" value="1" type="submit">Accept</button>
        <div class="or"></div>
        <button class="ui red button" name="engineer_status" value="2" type="submit">Escalate</button>
      </div>
    </form>
  </div>
</div>
@endif

@endsection
