<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Styles -->
{{--     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
--}}    
<link rel="stylesheet" href="{{ asset('css/semantic-ui.css') }}">
<link rel="stylesheet" href="{{ asset('css/alertify/main.css') }}">

<link rel="stylesheet" href="{{ asset('css/alertify/main.css') }}">

<link rel="stylesheet" href="{{ asset('css/alertify/alertify.css') }}">


<link rel="stylesheet" href="{{ asset('css/calendar.min.css') }}">

<link rel="stylesheet" href="{{ asset('css/Semantic-UI-Alert.css') }}">

<link rel="stylesheet" href="{{ asset('css/dataTables.semanticui.min.css') }}">



{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/css/datepicker.min.css" /> --}}

<script src="{{ asset('js/jquery.min.js') }}"></script>






<style type="text/css">
 body {
  background-color: #DADADA;
}
.ui.menu .item img.logo {
  margin-right: 1.5em;
}
.main.container {
  margin-top: 7em;
}
.wireframe {
  margin-top: 2em;
}
.ui.footer.segment {
  margin: 5em 0em 0em;
  padding: 5em 0em;
}
</style>

</head>
<body>
  <div id="app"> </div>
  <div class="ui fixed inverted menu">
    <div class="ui container">
      @include('elements.headerMenu')
      <div class="right menu">
        <div class="item">
          <div class="ui transparent inverted icon input">
            Welcome, {{ Auth::user()->name }}
          </div>
        </div>
        <a class="item" href="{{ route('logout') }}"
        onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
        Logout
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
      </form>

      <input type="hidden" id="authUserId" value="{{ Auth::user()->id }}">
    </div>
  </div>
</div>
<div class="ui main container">


 @yield('content')
</div>

<!-- Scripts -->
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('js/semantic-ui.js') }}"></script>
<script src="{{ asset('js/calendar.min.js') }}"></script>
<script src="{{ asset('js/alertify.js') }}"></script>

<script src="{{ asset('js/datatable/jquery.dataTables.min.js') }}"></script>

<script src="{{ asset('js/datatable/dataTables.semanticui.min.js') }}"></script>

<script src="{{ asset('js/ajaxform.min.js') }}"></script>

<script src="{{ asset('js/Semantic-UI-Alert.js') }}"></script>

<script src="{{ asset('js/jquery.easing.min.js') }}"></script>

<script src="{{ asset('js/jquery.easypiechart.min.js') }}"></script>

<script src="{{ asset('js/custom.js') }}"></script>


</body>
</html>
