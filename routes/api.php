<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/geography','ApiController@geography');

Route::get('/project','ApiController@project');

Route::get('/engineers','ApiController@engineers');

Route::get('/supervisors','ApiController@supervisors');

Route::get('/factors','ApiController@factors');

Route::post('/saveScoreDrafts','ApiController@saveScoreDrafts');

Route::get('/scoreResult/{evaluation}','ApiController@scoreResult');

Route::post('/saveIdleTime','ApiController@saveIdleTime');






