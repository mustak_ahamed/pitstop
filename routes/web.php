<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('/', function () {
	return view('auth.login');
});

Route::resource('users', 'UserController');

Route::resource('roles', 'RoleController');

Route::resource('permissions', 'PermissionController');

Route::resource('posts', 'PostController');

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/evaluation', 'HomeController@evaluation');

// Route::post('/evaluation', 'HomeController@submitEvaluation');

// Route::post('/saveScore', 'HomeController@saveScore');

// Route::get('/myEvaluation/{evaluation}', 'HomeController@myEvaluation');

// Route::post('/engEvaluationSubmit', 'HomeController@engEvaluationSubmit');

Route::get('/qa', 'HomeController@qa');

// Route::get('/eng', 'HomeController@eng');

Route::get('/tl', 'HomeController@tl');

// Route::get('/pending', 'HomeController@pending');

// Route::get('/qaEvaluated', 'HomeController@qaEvaluated');

// Route::get('/coach/{evaluation}', 'HomeController@coach');

// Route::post('/coach', 'HomeController@saveCoach');


Route::group(['prefix' => 'qa'], function() {

	Route::get('/evaluation', 'QaController@evaluation');

	Route::get('evaluated', 'QaController@evaluated');

	Route::get('coach/{evaluation}', 'QaController@coach');

	Route::post('coach', 'QaController@saveCoach');

	Route::post('/evaluation', 'QaController@submitEvaluation');

	Route::post('/saveScore', 'QaController@saveScore');



});

Route::group(['prefix' => 'engineer'], function() {

	Route::get('/dashboard', 'EngineerController@dashboard');

	Route::get('pending', 'EngineerController@pending');

	Route::get('myEvaluation/{evaluation}', 'EngineerController@myEvaluation');

	Route::post('/engEvaluationSubmit', 'EngineerController@engEvaluationSubmit');

	Route::get('/analytics', 'EngineerController@analytics');


});

Route::group(['prefix' => 'lead'], function() {

	Route::get('/dashboard', 'LeadController@dashboard');

	Route::get('/viewEvaluation/{evaluation}', 'LeadController@viewEvaluation');

	Route::get('/export', 'LeadController@exportEvaluation');



	
});