<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompletedToEvaluations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('evaluations', function (Blueprint $table) {
            $table->integer('completed')->nullable();
            $table->string('qa_comment')->nullable();
            $table->string('engineer_comment')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('evaluations', function (Blueprint $table) {
            $table->dropColumn('completed');
            $table->dropColumn('qa_commment');
            $table->dropColumn('engineer_comment');
            
        });
    }
}
