<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoachingDurationToEvaluations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('evaluations', function (Blueprint $table) {
           
            $table->time('coaching_duration')->default('00:00:00');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('evaluations', function (Blueprint $table) {
           
            $table->dropColumn('coaching_duration');
            
        });
    }
}
