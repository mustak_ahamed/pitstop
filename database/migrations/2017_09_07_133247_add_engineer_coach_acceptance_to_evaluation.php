<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEngineerCoachAcceptanceToEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('evaluations', function (Blueprint $table) {
            $table->integer('engineer_coach_acceptance')->default(0);
                        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('evaluations', function (Blueprint $table) {
            $table->dropColumn('engineer_coach_acceptance');
                        
        });
    }
}
