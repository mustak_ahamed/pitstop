<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('geography_id');
            $table->integer('language_id');
            $table->integer('lob_id');
            $table->dateTime('monitoring_date');
            $table->dateTime('date_of_evaluation');
            $table->string('call_id');
            $table->dateTime('call_date');
            $table->integer('engineer_id');
            $table->integer('supervisor_id');
            $table->integer('qa_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
