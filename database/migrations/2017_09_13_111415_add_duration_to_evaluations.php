<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDurationToEvaluations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
          Schema::table('evaluations', function (Blueprint $table) {
           
            $table->time('call_duration')->after('call_id')->default('00:00:00');
            $table->time('evaluation_duration')->default('00:00:00');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('evaluations', function (Blueprint $table) {
           
            $table->dropColumn('call_duration');
            $table->dropColumn('evaluation_duration');
            
        });
    }
}
