<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompletedAtToEvaluationScores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('evaluation_scores', function (Blueprint $table) {
            $table->integer('completed');
            $table->integer('engineer_status');
            $table->string('qa_commment');
            $table->string('engineer_comment');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
          Schema::table('evaluation_scores', function (Blueprint $table) {
            $table->dropColumn('completed');
            $table->dropColumn('engineer_status');
            $table->dropColumn('qa_commment');
            $table->dropColumn('engineer_comment');
            
        });
    }
}
