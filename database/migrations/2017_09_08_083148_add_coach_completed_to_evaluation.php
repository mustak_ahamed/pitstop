<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoachCompletedToEvaluation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('evaluations', function (Blueprint $table) {
            $table->integer('coach_completed')->default(0);
            $table->string('coaching_input')->nullable();
            $table->integer('qa_coach_acceptance')->default(0);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
       Schema::table('evaluations', function (Blueprint $table) {
        $table->dropColumn('coach_completed');
        $table->dropColumn('coaching_input');
        $table->dropColumn('qa_coach_acceptance');
        
    });
   }
}
