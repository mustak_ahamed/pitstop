var idleTime = 0;


$(document).ready(function() {

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })

   var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {

      if(idleTime > 1) {
        saveIdleTime(idleTime,0);
      }

      idleTime = 0;
    });
    $(this).keypress(function (e) {

      if(idleTime > 1) {
        saveIdleTime(idleTime,0);
      }

      idleTime = 0;

    });


    $('select.dropdown').dropdown();
    $('.datepicker').calendar();
    $('.menu .item').tab();


    $('.ui.accordion').accordion();


    $('.datatable').DataTable({
     "bLengthChange": false,
     "bSort":false,
   });

    $('#upload_file').on('change', function () {

        //this.files[0].size gets the size of your file.
        var size_bytes = this.files[0].size;
        //alert(file_size);
        var file_name = $(this).val();
        var file_name_array = file_name.split('.');

        var extensions = [
        'pdf',
        'wav',
        'docx',
        'doc'
        ];
        if ($.inArray(file_name_array.pop().toLowerCase(), extensions) >= 0) {
            // $('.browse-btn').attr("disabled", true);

          }
          else {
            $(this).val('');
            alert("Please upload a valid file");
          }
        });
    var ct = 0;


    $('.prev1').on('click', function(e) {

      e.preventDefault();

      $('#factorList').animate('slow', function() {

        $('#factorList').transition('hide');
        $("#monitor").transition('fly right');

      });

    });


    $("form#evaluationForm").submit(function (e) {

      e.preventDefault();



      $(this).ajaxSubmit({

       beforeSubmit: function () {
        $('#evaluationForm').addClass('loading');

      },
      success: function (data) {
       if(data) {
        $('#evaluation_id').val(data);

        $('.loading').removeClass('loading');

        $('#monitor').animate('slow', function() {

          if (ct > 0) {
            $('#monitor').removeClass('transition visible');
            $('#monitor').addClass('transition hidden');

          }
          $('#monitor').css('display', 'none');

          $("#factorList").transition('fly right');
        // $('#social button').addClass('inverted blue');
        ct++;

      });
      }
    },

    error:function(jqXHR, exception) {

     $('.loading').removeClass('loading');
     console.log(jqXHR.responseText);
     var data = jqXHR.responseJSON;
     $.each( data , function( key, value ) {
      $.uiAlert({
          textHead: "Please fill all the details in the form", // header
          text: value[0], // Text
          bgcolor: '#DB2828', // background-color
          textcolor: '#fff', // color
          position: 'top-right',// position . top And bottom ||  left / center / right
          icon: 'remove circle', // icon in semantic-UI
          time:10
        });
    });

   },

 });
      return false;
    });


    $("form#scoreForm").submit(function (e) {

      e.preventDefault();
      $(this).ajaxSubmit({

       beforeSubmit: function () {
        $('#scoreForm').addClass('loading');

      },

      success: function (data) {
        $('.loading').removeClass('loading');
      // alert('saved');
      alertify.confirm('Success', 'Evaluation has been save successfully!', function(){ window.location = "evaluation";
    }
    , function(){ window.location = "evaluated";}).set('labels', {ok:'Evaluate More', cancel:'Return Back'});
    },

  });
      return false;
    });


  });

function timerIncrement() {
  idleTime = idleTime + 1;
  console.log(idleTime);
    if (idleTime > 10) { // idle for 10 minutes
      
        saveIdleTime(idleTime,1);

      }
    }

    function saveIdleTime(idleTime,status) {
      var userId = $('#authUserId').val();
      var time = idleTime
      $.ajax({
        url:"/api/saveIdleTime",
        method: 'POST',
        dataType: 'json',
        data: {
          userId:userId,
          time:time,
        },
        success: function(data) {
         if(status == 1) {
           $('form#logout-form').submit();
         }
       }
     });
    }