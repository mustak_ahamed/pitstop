const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .copy('node_modules/semantic-ui-css/semantic.min.css','public/css/semantic.min.css')
   .copy('node_modules/semantic-ui-css/semantic.min.js','public/js/semantic.min.js')
   .copy('node_modules/semantic-ui-css/themes/default/assets/fonts/icons.woff2','public/css/themes/default/assets/fonts/icons.woff2')
   .copy('node_modules/semantic-ui-css/themes/default/assets/fonts/icons.woff','public/css/themes/default/assets/fonts/icons.woff')
   .copy('node_modules/semantic-ui-css/themes/default/assets/fonts/icons.ttf','public/css/themes/default/assets/fonts/icons.ttf')
   .copy('node_modules/semantic-ui-calendar/dist/calendar.min.css','public/css/calendar.min.css')
   .copy('node_modules/semantic-ui-calendar/dist/calendar.min.js','public/js/calendar.min.js');
