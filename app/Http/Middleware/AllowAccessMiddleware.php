<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AllowAccessMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {        
        if (Auth::user()->hasPermissionTo('Administer roles & permissions')) {
            return $next($request);
        }

        if ($request->is('qa/*')) {
            if (!Auth::user()->hasRole('QA')) {
                abort('401');
            } else {
                return $next($request);
            }
        }

        if ($request->is('engineer/*')) {
            if (!Auth::user()->hasRole('Engineer')) {
                abort('401');
            } else {
                return $next($request);
            }
        }

        if ($request->is('lead/*')) {
            if (!Auth::user()->hasRole('Lead')) {
                abort('401');
            } else {
                return $next($request);
            }
        }
       

        return $next($request);
    }
}