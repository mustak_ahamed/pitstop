<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Language;

use App\LOB;

use App\Evaluation;

use App\EvaluationFactor;

use Carbon\Carbon;

use App\EvaluationScoreDraft;

use App\EvaluationScore;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $uid;

    public function __construct()
    {
        $this->uid = substr(uniqid(), 6);

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function evaluation()
    {
        $languages = Language::all();
        $lob = LOB::all();
        return view('form',compact('languages','lob'));
    }

    public function myEvaluation($evaluation)
    {

        $evaluationDetail = Evaluation::with(['Geography','Project','Language','LOB','Engineer','Supervisor','Qa','EvaluationScore.EvaluationFactor'])->find($evaluation);
        // dd($evaluationDetail);

        $scoreDetails = $this->scoreCalculation($evaluation,'1');

        return view('myEvaluation',compact('evaluationDetail','scoreDetails'));
    }

    public function qa()
    {
        return view('qaDashboard');
    }

    public function eng()
    {
     $evaluations = Evaluation::where(['completed'=>true,'coach_completed'=>1])->whereNotNull('engineer_status')->with(['Geography','Project','Supervisor','Qa'])->get();
     return view('engDashboard',compact('evaluations'));
 }

 public function tl()
 {
    return view('tlDashboard');
}

public function submitEvaluation(Evaluation $evaluation,Request $request,EvaluationScoreDraft $evaluationScoreDraft)
{
        // dd($request->input());
    $errors = $this->validate(request(),[
        'geography_id' =>'required',
        'project_id' =>'required',
        'language_id' =>'required',
        'lob_id'=>'required',
        'call_id' =>'required',
        'call_date'=>'required|date',
        'hours'=>'required',
        'minutes'=>'required',
        'engineer_id' =>'required',
        'supervisor_id'=>'required',
        'evaluationDoc'=>'required'
        ]);

    $factors = EvaluationFactor::all();

    $hours = ($request->input('hours') > 10) ? $request->input('hours') : "0".$request->input('hours');
    $minutes = ($request->input('minutes') > 10) ? $request->input('minutes') : "0".$request->input('minutes');
    $call_duration =  $hours .":".  $minutes .":". "00";

    if(!$request->input('evaluation_id')) {

        $evaluation = new Evaluation;
        $evaluation->geography_id = $request->input('geography_id');
        $evaluation->project_id = $request->input('project_id');
        $evaluation->language_id = $request->input('language_id');
        $evaluation->lob_id = $request->input('lob_id');
        $evaluation->monitoring_date = Carbon::parse($request->input('monitoring_date'));
        $evaluation->date_of_evaluation = Carbon::parse($request->input('date_of_evaluation'));
        $evaluation->call_id = $request->input('call_id');
        $evaluation->call_date = Carbon::parse($request->input('call_date'));
        $evaluation->call_duration = $call_duration;
        $evaluation->engineer_id = $request->input('engineer_id');
        $evaluation->supervisor_id = $request->input('supervisor_id');
        $evaluation->qa_id = $request->input('qa_id');

        if($request->hasFile('evaluationDoc'))
        {

            $file = $request->file('evaluationDoc');
            $uid = $this->uid;
            $fileName = 'evaluationDoc' . '-' . Carbon::now()->toDateString() . '-' . $uid . '.';
            $fileExtension = $file->getClientOriginalExtension();

            $destinationPath = 'uploads';
            $filePath = $file->move($destinationPath,$fileName.$fileExtension);

            if($filePath) {
                $evaluation->file_path = $filePath;
            }
        }

        if($evaluation->save()) {

            foreach ($factors as $key => $factor) {
               $evaluationScoreDraft = new EvaluationScoreDraft;
               $evaluationScoreDraft->evaluation_factor_id = $factor->id;
               $evaluationScoreDraft->comment = '';
               $evaluationScoreDraft->evaluation_id = $evaluation->id;
               $evaluationScoreDraft->save();

           }
           return $evaluation->id;
       }
   } 
   else 
   {
    $evaluationExistance = Evaluation::where("id",$request->input('evaluation_id'))->first();

    $evaluationExistance->geography_id = $request->input('geography_id');
    $evaluationExistance->project_id = $request->input('project_id');
    $evaluationExistance->language_id = $request->input('language_id');
    $evaluationExistance->lob_id = $request->input('lob_id');
    $evaluationExistance->monitoring_date = Carbon::parse($request->input('monitoring_date'));
    $evaluationExistance->date_of_evaluation = Carbon::parse($request->input('date_of_evaluation'));
    $evaluationExistance->call_id = $request->input('call_id');
    $evaluationExistance->call_date = Carbon::parse($request->input('call_date'));
    $evaluationExistance->call_duration = $call_duration;
    $evaluationExistance->engineer_id = $request->input('engineer_id');
    $evaluationExistance->supervisor_id = $request->input('supervisor_id');

    if($request->hasFile('evaluationDoc'))
        {

            $file = $request->file('evaluationDoc');
            $uid = $this->uid;
            $fileName = 'evaluationDoc' . '-' . Carbon::now()->toDateString() . '-' . $uid . '.';
            $fileExtension = $file->getClientOriginalExtension();

            $destinationPath = 'uploads';
            $filePath = $file->move($destinationPath,$fileName.$fileExtension);

            if($filePath) {
                $evaluation->file_path = $filePath;
            }
        }


    $evaluationExistance->update();
}
}

public function saveScore(Request $request,EvaluationScore $evaluationScore) 
{
    $factors = EvaluationFactor::all();
    foreach ($factors as $key => $factor) {
        $evaluationScore = new EvaluationScore;
        $evaluationScore->evaluation_factor_id = $factor->id;
        $evaluationScore->score = $request->input('factor_'.$factor->id);
        $evaluationScore->comment = $request->input('comment_'.$factor->id);
        $evaluationScore->evaluation_id = $request->input('evaluation_id');
        $evaluationScore->save();

    }

    $evaluationExistance = Evaluation::where("id",$request->input('evaluation_id'))->first();
    $evaluationExistance->completed = true;
    $evaluationExistance->qa_comment = $request->input('qa_comment');
    $evaluationExistance->evaluation_duration = $request->input('evaluation_duration');    
    $evaluationExistance->update();


    return $evaluationExistance->id;
}

public function pending() 
{
    $evaluations = Evaluation::where(['completed'=>true, 'engineer_status'=>0,'coach_completed'=>1])->with(['Geography','Project','Supervisor','Qa'])->get();
    return view('pendingSignoff',compact('evaluations'));
}
public function engEvaluationSubmit(Request $request) 
{
    $evaluationExistance = Evaluation::where("id",$request->input('evaluation_id'))->first();
    $evaluationExistance->engineer_comment = $request->input('engineer_comment');
    $evaluationExistance->engineer_coach_acceptance = $request->input('engineer_coach_acceptance');
    $evaluationExistance->engineer_status = $request->input('engineer_status');

    $evaluationExistance->update();

    return redirect('/eng');
}

public function qaEvaluated() {
    $evaluations = Evaluation::where(['completed'=>true, 'coach_completed'=>0,'qa_id'=> Auth::user()->id])->with(['Project','Engineer'])->get();
    // dd($evaluations);
    return view('completedEvaluations',compact('evaluations'));
}

public function coach($evaluation) {
    $evaluationDetail = Evaluation::with(['Geography','Project','Language','LOB','Engineer','Supervisor','Qa','EvaluationScore.EvaluationFactor'])->find($evaluation);
        // dd($evaluationDetail);
    $scoreDetails = $this->scoreCalculation($evaluation,'1');

    return view('coach',compact('evaluationDetail','scoreDetails'));
}

// public function saveCoach(Request $request) 
// {
//     $evaluationExistance = Evaluation::where("id",$request->input('evaluation_id'))->first();
//     $evaluationExistance->coaching_input = $request->input('coaching_input');
//     $evaluationExistance->qa_coach_acceptance = $request->input('qa_coach_acceptance');
//     $evaluationExistance->coach_completed = true;

//     $evaluationExistance->update();

//     return redirect('/qaEvaluated');
// }

}
