<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Geography;

use App\Project;

use App\Engineer;

use App\Supervisor;

use App\EvaluationFactor;

use App\EvaluationScoreDraft;

use App\TimeTracker;


class Apicontroller extends Controller
{
    //
   
    public function geography()
    {
        $geography['data'] = Geography::all();

        return \Response::json($geography);
    }
    public function project()
    {
        $project['data'] = Project::all();

        return \Response::json($project);
    }

    public function engineers()
    {
        $engineers['data'] = Engineer::with('Supervisor')->get();

        return \Response::json($engineers);
    }

    public function supervisors()
    {
        $supervisors['data'] = Supervisor::all();

        return \Response::json($supervisors);
    }
    public function factors()
    {
        $factors['data'] = EvaluationFactor::all();

        return \Response::json($factors);
    }

    public function saveScoreDrafts(Request $request,EvaluationScoreDraft $evaluationScoreDraft) 
    {
        $evaluationScoreExistance = EvaluationScoreDraft::where(['evaluation_factor_id'=>$request->input('factor_id'),
            'evaluation_id'=> $request->input('evaluation_id')])->first();

        if($evaluationScoreExistance) {
            $evaluationScoreExistance->score = $request->input('score');

            if( $request->input('comment')) {
                $evaluationScoreExistance->comment = $request->input('comment');
            }

        }

        $evaluationScoreExistance->update();

        return \Response::json($evaluationScoreExistance);
    }

    public function scoreResult($evaluation) {
       $scoreDetails = $this->scoreCalculation($evaluation,'2');

       return \Response::json($scoreDetails);

   }

   public function saveIdleTime(Request $request) 
   { 

     $trackExistance = TimeTracker::where(['user_id'=> $request->input('userId')])->whereDate('created_at','=',date('Y-m-d'))->first();

     // dd($trackExistance);

     if(!$trackExistance) {

        $tracker = new TimeTracker;

        $tracker->user_id = $request->input('userId');
        $tracker->idleTime = $request->input('time');

        $tracker->save();

        return $tracker->id;

     } else {
        $trackExistance->idleTime = (intval($trackExistance->idleTime)) + intval($request->input('time'));

        $trackExistance->update();

        return $trackExistance->id;
     }

   }
}
