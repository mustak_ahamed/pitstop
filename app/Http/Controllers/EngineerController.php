<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Language;

use App\LOB;

use App\Evaluation;

use App\EvaluationFactor;

use Carbon\Carbon;

use App\EvaluationScoreDraft;

use App\EvaluationScore;

use Auth;

class EngineerController extends Controller
{
    //

	public function __construct()
	{
		$this->middleware(['auth','allowAccess']);
	}

	public function dashboard()
	{
		$evaluations = Evaluation::where(['completed'=>true,'coach_completed'=>1])->whereNotNull('engineer_status')->with(['Geography','Project','Supervisor','Qa'])->get();

		$pendingEvaluations = Evaluation::where(['completed'=>true, 'engineer_status'=>0,'coach_completed'=>1])->with(['Geography','Project','Supervisor','Qa'])->count();

		
		return view('engineer.engDashboard',compact('evaluations','pendingEvaluations'));
	}

	public function pending() 
	{
		$evaluations = Evaluation::where(['completed'=>true, 'engineer_status'=>0,'coach_completed'=>1])->with(['Geography','Project','Supervisor','Qa'])->get();

		// $pendingEvaluations = $evaluations->count();


		return view('engineer.pendingSignoff',compact('evaluations'));
	}

	public function myEvaluation($evaluation)
	{

		$evaluationDetail = Evaluation::with(['Geography','Project','Language','LOB','Engineer','Supervisor','Qa','EvaluationScore.EvaluationFactor'])->find($evaluation);
        // dd($evaluationDetail);

		/* Retireive score from sEvaluationScores table (param diffrentiater 1)*/
		$scoreDetails = $this->scoreCalculation($evaluation,'1');

		return view('engineer.myEvaluation',compact('evaluationDetail','scoreDetails'));
	}

	public function engEvaluationSubmit(Request $request) 
	{
		$evaluationExistance = Evaluation::where("id",$request->input('evaluation_id'))->first();
		$evaluationExistance->engineer_comment = $request->input('engineer_comment');
		$evaluationExistance->engineer_coach_acceptance = $request->input('engineer_coach_acceptance');
		$evaluationExistance->engineer_status = $request->input('engineer_status');

		$evaluationExistance->update();

		return redirect('engineer/dashboard');
	}

	public function analytics(EvaluationScore $evaluation) {
	
			return view('engineer.analytics');

	}
}
