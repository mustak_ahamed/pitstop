<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /*Redirection logic based on the role*/
    public function authenticated(Request $request, $user)
    {
        //
        if($user->roles()->pluck('name')[0] == 'Admin') { 
            return redirect()->route('users.index');
        } else if($user->roles()->pluck('name')[0] == 'QA') {
            return redirect('qa/evaluated') ;
        } else if($user->roles()->pluck('name')[0] == 'Lead'){
            return redirect('lead/dashboard');
        } else if($user->roles()->pluck('name')[0] == 'Engineer'){
            return redirect('engineer/dashboard') ;
        } else {
            return redirect('/') ;
        }
    }
}
