<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Language;

use App\LOB;

use App\Evaluation;

use App\EvaluationFactor;

use Carbon\Carbon;

use App\EvaluationScoreDraft;

use App\EvaluationScore;

use Auth;

use Excel; 

class LeadController extends Controller
{
    //

	public function __construct()
	{
		$this->middleware(['auth','allowAccess']);

		$this->uid = substr(uniqid(), 6);
	}

	public function dashboard(EvaluationScore $evaluation) {
		$evaluations = Evaluation::where(['completed'=>true,'coach_completed'=>1])->whereNotNull('engineer_status')->with(['Geography','Project','Supervisor','Qa','Engineer'])->get();

		return view('lead.tlDashboard',compact('evaluations'));

	}

	public function analytics(EvaluationScore $evaluation) {
		$evaluations = Evaluation::where(['completed'=>true,'coach_completed'=>1])->whereNotNull('engineer_status')->with(['Geography','Project','Supervisor','Qa','Engineer'])->get();

		return view('lead.analytics',compact('evaluations'));

	}

	public function viewEvaluation($evaluation)
	{

		$evaluationDetail = Evaluation::with(['Geography','Project','Language','LOB','Engineer','Supervisor','Qa','EvaluationScore.EvaluationFactor'])->find($evaluation);
        // dd($evaluationDetail);

		$scoreDetails = $this->scoreCalculation($evaluation,'1');

		return view('lead.viewEvaluation',compact('evaluationDetail','scoreDetails'));
	}

	public function exportEvaluation() 
	{

		
		$uid = $this->uid;

		Excel::create('evaluationDump' . '-' . $uid, function($excel) {

			$excel->sheet('Evaluation sheet', function($sheet) {

				$evaluations = Evaluation::where(['completed'=>true,'coach_completed'=>1])->whereNotNull('engineer_status')->with(['Geography','Project','Supervisor','Qa','Engineer'])->get();

				$sheet->loadView('lead.exportEvaluation')->with('evaluations', $evaluations);

			});

		})->download('xls');
	}
}
