<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Evaluation;

use App\EvaluationScore;

use App\EvaluationScoreDraft;

use App\EvaluationFactor;

use DB;


class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function scoreCalculation($evaluation,$model) 
	{
		if($model == 1) {
			$evaluationDetail = EvaluationScore::where('evaluation_id',$evaluation)->with('EvaluationFactor')->whereNotNull('score')->get();
		} else if ($model == 2) {
			$evaluationDetail = EvaluationScoreDraft::where('evaluation_id',$evaluation)->with('EvaluationFactor')->whereNotNull('score')->get();
		} 

		$callScore = $emailScore = $ceScore = [];
			/* the score value -1 is NA option */
		foreach ($evaluationDetail as $key => $evalScore) {
			if($evalScore->EvaluationFactor->type == 'call') {
				array_push($callScore, $evalScore->score);
			} else if($evalScore->EvaluationFactor->type == 'email') {
				array_push($emailScore, $evalScore->score);
			} else if($evalScore->EvaluationFactor->type == 'ce') {
				array_push($ceScore, $evalScore->score);
			}
		}

		/*Maximum score value is 2. So we have multiplied the factors count with 2*/
		$callFactorsMax =  count($callScore)*2;
		$emailFactorsMax =  count($emailScore)*2;
		$ceFactorsMax = count($ceScore)*2;

		$totalCallScore = array_sum($callScore);

		$totalEmailScore = array_sum($emailScore);

		$totalCEScore = array_sum($ceScore);


		$callScoreInPer = !empty($callFactorsMax) ? round(($totalCallScore/$callFactorsMax) * 100,2) : 0;

		$emailScoreInPer = !empty($emailFactorsMax) ? round(($totalEmailScore/$emailFactorsMax) * 100,2) : 0;

		$ceScoreInPer = !empty($ceFactorsMax) ? round(($totalCEScore/$ceFactorsMax) * 100,2) : 0;

		$totalScores = round((($callScoreInPer/100) * 35) + (($emailScoreInPer/100) * 35) + (($ceScoreInPer/100) * 30),2);

	
		return ['callScores' => $callScoreInPer,'emailScores'=>$emailScoreInPer,'ceScores' => $ceScoreInPer,
		'totalScores' =>$totalScores ];
	}
}
