<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Geography extends Model
{
    //
	protected $table = 'geography';

	public function projects()
	{
		return $this->hasMany('App\Project');
	}

}
