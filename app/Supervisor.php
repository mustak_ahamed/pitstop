<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supervisor extends Model
{
    //
    public function supervisor(){
    	return $this->hasMany('App\Engineer','supervisor_id');
    }
}
