<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class EvaluationScoreDraft extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function evaluationFactor(){
    	return $this->belongsTo('App\EvaluationFactor','evaluation_factor_id');
    }
}
