<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engineer extends Model
{
    //

    public function supervisor(){
    	return $this->belongsTo('App\Supervisor','supervisor_id');
    }
}
