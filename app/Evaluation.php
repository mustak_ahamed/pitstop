<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    //
	protected $dates = ['date_of_evaluation'];

    public function language(){
    	return $this->belongsTo('App\Language','language_id');
    }

    public function geography(){
    	return $this->belongsTo('App\Geography','geography_id');
    }

    public function lob(){
    	return $this->belongsTo('App\LOB','lob_id');
    }

    public function engineer(){
    	return $this->belongsTo('App\Engineer','engineer_id');
    }

     public function supervisor(){
    	return $this->belongsTo('App\Supervisor','supervisor_id');
    }

    public function qa() {
    	return  $this->belongsTo('App\User','qa_id');
    }

    public function project() {
    	return  $this->belongsTo('App\Project','project_id');
    }
     public function evaluationScore() {
    	return  $this->hasMany('App\EvaluationScore','evaluation_id');
    }
}
