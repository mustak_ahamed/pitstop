<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluationScore extends Model
{
    //
      public function evaluationFactor(){
    	return $this->belongsTo('App\EvaluationFactor','evaluation_factor_id');
    }
}
